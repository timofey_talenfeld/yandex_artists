package ru.yandex.yandexartists.type;

import android.app.Activity;

/**
 * Created by root on 7/20/16.
 */
public interface OnSelectedItem<T> {
    void onSelectedItem(T item);
}
