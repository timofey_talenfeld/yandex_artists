package ru.yandex.yandexartists.utils;

import android.content.Context;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

import ru.yandex.yandexartists.R;
import ru.yandex.yandexartists.bean.Artist;

/**
 * Created by root on 4/25/16.
 */
public class StringUtils {

    public static String getGenresString(String[] array) {

        StringBuilder sb = new StringBuilder();
        for(String genre : array) {
            sb.append(' ');
            sb.append(genre);
            sb.append(',');
        }

        String genres = null;
        if(array.length > 0) {
            genres = sb.substring(1, sb.length() - 1);
        } else {
            genres = sb.toString();
        }
        return genres;
    }

    public static String getCounterInfoString(Context context, Artist artist) {
        StringBuilder counterBuilder = new StringBuilder();

        counterBuilder.append(context.getResources().getQuantityString(R.plurals.albums, artist.getAlbums(), artist.getAlbums()));
        counterBuilder.append(", ");
        counterBuilder.append(context.getResources().getQuantityString(R.plurals.tracks, artist.getTracks(), artist.getTracks()));

        return counterBuilder.toString();
    }

}
