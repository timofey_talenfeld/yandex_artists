package ru.yandex.yandexartists.function;

/**
 * Created by root on 4/25/16.
 */

public interface Consumer<I> {
    void function(I value);
}

