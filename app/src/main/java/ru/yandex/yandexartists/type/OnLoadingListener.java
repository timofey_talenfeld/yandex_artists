package ru.yandex.yandexartists.type;

/**
 * Created by root on 7/19/16.
 */
public interface OnLoadingListener<T> {
    void onStartFirstLoading();
    void onEndFirstLoading(T result, boolean requireRefresh);
    void onFailedFirstLoading();
    void onStartLoading();
    void onEndLoading(T result);
    void onFailedLoading();

}
