package ru.yandex.yandexartists.database;

import ru.yandex.yandexartists.bean.Cover;

/**
 * Created by root on 7/21/16.
 */
public class ArtistDbSchema {

    public static final class ArtistTable {
        public static final String NAME = "artists";

        public static final class Cols {
            public static final String NAME = "name";
            public static final String GENRES = "genres";
            public static final String TRACKS = "tracks";
            public static final String ALBUMS = "albums";
            public static final String LINK = "link";
            public static final String DESCRIPTION = "description";
            public static final String BIG_COVER = "big_cover";
            public static final String SMALL_COVER = "small_cover";
        }

    }

}
