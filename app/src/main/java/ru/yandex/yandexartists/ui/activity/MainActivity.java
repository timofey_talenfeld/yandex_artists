package ru.yandex.yandexartists.ui.activity;

import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import ru.yandex.yandexartists.R;
import ru.yandex.yandexartists.asynctask.UpdateDatabaseTask;
import ru.yandex.yandexartists.bean.Artist;
import ru.yandex.yandexartists.database.ArtistsStorage;
import ru.yandex.yandexartists.receiver.HeadsetReceiver;
import ru.yandex.yandexartists.type.OnLoadingListener;
import ru.yandex.yandexartists.type.OnSelectedItem;
import ru.yandex.yandexartists.ui.fragment.AboutFragment;
import ru.yandex.yandexartists.ui.fragment.ArtistInfoFragment;
import ru.yandex.yandexartists.ui.fragment.ArtistsListFragment;
import ru.yandex.yandexartists.ui.fragment.LoadingFragment;
import ru.yandex.yandexartists.utils.AnimationUtils;

public class MainActivity extends AppCompatActivity implements OnLoadingListener<Artist[]>, OnSelectedItem<Artist> {

    private Artist[] artists;

    private HeadsetReceiver headsetReceiver;

    private LoadingFragment loadingFragment;
    private ArtistsListFragment artistsListFragment;
    private ArtistInfoFragment artistInfoFragment;
    private Toolbar toolbar;

    private AsyncTask<Void, Void, Void> updateDatabaseTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupToolbar();
        getSupportFragmentManager().addOnBackStackChangedListener(onBackStackChangedListener());
        if(savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.content_fragment, loadingFragment = new LoadingFragment()).commit();
        } else {
            showToolbar();
        }

        headsetReceiver = new HeadsetReceiver();
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
        registerReceiver(headsetReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(updateDatabaseTask != null) {
            updateDatabaseTask.cancel(true);
        }
        unregisterReceiver(headsetReceiver);
    }

    private FragmentManager.OnBackStackChangedListener onBackStackChangedListener() {
        return () -> {
            FragmentManager manager = getSupportFragmentManager();
            if (manager != null) {
                int count = manager.getBackStackEntryCount();

                Log.d("artists", "listener count: " + count);

                if (count != 0) {
                    Fragment fragment = manager.getFragments().get(count);
                    fragment.onResume();
                }
            }
        };
    }

    private void setupToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener((v) -> onBackPressed());
    }

    private void composeEmail() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setData(Uri.parse("mailto:"));
        intent.setType("*/*");
        intent.putExtra(Intent.EXTRA_EMAIL, getResources().getStringArray(R.array.emails));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    private void showToolbar() {
        AnimationUtils.movingAnimation(toolbar, 0, 0, -250, 0, 700, (animation) -> toolbar.setVisibility(View.VISIBLE), (animation) -> {});
    }

    @Override
    public void onBackPressed() {

        int count = getSupportFragmentManager().getBackStackEntryCount();

        Log.d("artists", "back sttack count: " + count);

        if (count <= 1) {
            getSupportActionBar().setTitle(getResources().getString(R.string.artists_list));
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            super.onBackPressed();
        } else {
            getSupportFragmentManager().popBackStack();
        }

    }

    private void hideLoadingAnimation(boolean requireRefresh) {

        new Handler().postDelayed(() -> {
            loadingFragment.setLoadingRate(100);
            new Handler().postDelayed(() -> {
                attachArtistsListFragment(requireRefresh);
                showToolbar();
            }, 500);

        }, 500);
    }

    private void attachArtistsListFragment(boolean requireRefresh) {

        Bundle fragmentArgs = new Bundle();
        fragmentArgs.putSerializable(Artist[].class.getCanonicalName(), artists);
        fragmentArgs.putSerializable("requireRefresh", requireRefresh);
        artistsListFragment = new ArtistsListFragment();
        artistsListFragment.setArguments(fragmentArgs);
        getSupportFragmentManager().beginTransaction().replace(R.id.content_fragment, artistsListFragment).commit();
    }

    @Override
    public void onStartFirstLoading() {

    }

    @Override
    public void onEndFirstLoading(Artist[] result, boolean requireRefresh) {
        artists = result;
        hideLoadingAnimation(requireRefresh);
    }

    @Override
    public void onFailedFirstLoading() {
        hideLoadingAnimation(false);
        onFailedLoading();
    }


    @Override
    public void onStartLoading() {

    }

    @Override
    public void onEndLoading(Artist[] result) {
        updateDatabaseTask = new UpdateDatabaseTask(result, getApplicationContext()).execute();
    }

    @Override
    public void onFailedLoading() {
        Toast.makeText(MainActivity.this, getResources().getString(R.string.error_loading), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSelectedItem(Artist artist) {

        Bundle fragmentArgs = new Bundle();
        fragmentArgs.putSerializable(Artist.class.getCanonicalName(), artist);

        attachFragment(new ArtistInfoFragment(), R.id.content_fragment, fragmentArgs, ArtistInfoFragment.class.getCanonicalName());
    }

    private void attachFragment(Fragment fragment, int id, Bundle bundle, String backStackName) {

        if(bundle != null) {
            fragment.setArguments(bundle);
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction().add(id, fragment);
        if(backStackName != null) {
            transaction.addToBackStack(backStackName);
        }
        transaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about_menu_item:
                attachFragment(new AboutFragment(), R.id.content_fragment, null, AboutFragment.class.getCanonicalName());
                return true;
            case R.id.feedback_menu_item:
                composeEmail();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
