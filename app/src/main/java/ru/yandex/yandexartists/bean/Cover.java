package ru.yandex.yandexartists.bean;

import java.io.Serializable;

/**
 * Created by root on 4/25/16.
 */
public class Cover implements Serializable {

    private String big;
    private String small;

    public Cover() {

    }

    public String getBig() {
        return big;
    }

    public void setBig(String big) {
        this.big = big;
    }

    public String getSmall() {
        return small;
    }

    public void setSmall(String small) {
        this.small = small;
    }
}
