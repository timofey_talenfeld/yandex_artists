package ru.yandex.yandexartists.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.yandex.yandexartists.R;
import ru.yandex.yandexartists.utils.AnimationUtils;

/**
 * Created by root on 7/20/16.
 */
public class AboutFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.about_fragment, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        enableHomeButton(true);
        AnimationUtils.movingAnimation(getView(), 1000, 0, 0, 0, 700, (animation) -> {}, (animation) -> {});
    }

    @Override
    public void onResume() {
        super.onResume();
        setActionBarTitle(R.string.about);
    }

    @Override
    public void onStop() {
        super.onStop();
        AnimationUtils.movingAnimation(getView(), 0, 1000, 0, 0, 700, (animation) -> {}, (animation) -> {});
    }

}
