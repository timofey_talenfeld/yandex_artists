package ru.yandex.yandexartists.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import ru.yandex.yandexartists.bean.Artist;
import ru.yandex.yandexartists.database.ArtistsStorage;
import ru.yandex.yandexartists.function.Consumer;

/**
 * Created by root on 7/21/16.
 */
public class LoadFromDatabaseTask extends AsyncTask<Void, Void, Artist[]> {

    private Context context;
    private Consumer<Artist[]> callback;

    public LoadFromDatabaseTask(Context context, Consumer<Artist[]> callback) {
        this.callback = callback;
        this.context = context.getApplicationContext();
    }

    @Override
    protected Artist[] doInBackground(Void... params) {
        return ArtistsStorage.getInstance(context).getArtists();
    }

    @Override
    protected void onPostExecute(Artist[] artists) {
        callback.function(artists);
    }
}
