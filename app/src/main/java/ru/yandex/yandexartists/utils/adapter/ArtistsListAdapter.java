package ru.yandex.yandexartists.utils.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import ru.yandex.yandexartists.R;
import ru.yandex.yandexartists.bean.Artist;
import ru.yandex.yandexartists.type.OnSelectedItem;
import ru.yandex.yandexartists.utils.StringUtils;

/**
 * Created by root on 4/25/16.
 */
public class ArtistsListAdapter extends BaseAdapter {

    private final OnSelectedItem<Artist> activity;
    private final Artist[] artists;

    public ArtistsListAdapter(OnSelectedItem<Artist> activity, Artist[] artists) {

        this.activity = activity;
        this.artists = artists;
    }

    @Override
    public int getCount() {
        return artists.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final View rowView = ((Activity)activity).getLayoutInflater().inflate(R.layout.artist_item, null);

        Picasso.with(((Activity)activity).getApplicationContext()).load(artists[position].getCover().getSmall()).centerCrop().fit().into((ImageView) rowView.findViewById(R.id.image));

        ((TextView) rowView.findViewById(R.id.name)).setText(artists[position].getName());

        ((TextView) rowView.findViewById(R.id.genres)).setText(StringUtils.getGenresString(artists[position].getGenres()));

        ((TextView) rowView.findViewById(R.id.counter)).setText(StringUtils.getCounterInfoString(((Activity)activity).getApplicationContext(), artists[position]));

        rowView.setOnClickListener((v) -> activity.onSelectedItem(artists[position]));

        return rowView;

    }

}
