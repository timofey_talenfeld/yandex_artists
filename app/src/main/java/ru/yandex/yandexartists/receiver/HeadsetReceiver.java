package ru.yandex.yandexartists.receiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ru.yandex.yandexartists.R;

/**
 * Created by root on 7/20/16.
 */
public class HeadsetReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
            int state = intent.getIntExtra("state", -1);
            switch (state) {
                case 0:
                    onUnplugHeadset(context);
                    break;
                case 1:
                    onPlugHeadset(context);
                    break;
                default:

            }
        }
    }

    private void onUnplugHeadset(Context context) {
        ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(1);
        ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(2);
    }

    private void onPlugHeadset(Context context) {
        createExternalNotification(context, "ru.yandex.music", 1);
        createExternalNotification(context, "ru.yandex.radio", 2);
    }

    private void createExternalNotification(Context context, String packageName, int id) {

        String appName;
        //int icon;
        try {
            //icon = context.getPackageManager().getApplicationInfo(packageName, PackageManager.GET_META_DATA).icon;
            appName = context.getPackageManager().getApplicationLabel(context.getPackageManager()
                    .getApplicationInfo(packageName, PackageManager.GET_META_DATA)).toString();

        } catch (PackageManager.NameNotFoundException e) {
            return;
        }

        Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, launchIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.yandex_logo)
                .setContentTitle(appName)
                .setContentText(context.getResources().getString(R.string.open_app, appName))
                .setContentIntent(resultPendingIntent);

        ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).notify(id, notificationBuilder.build());


    }
}
