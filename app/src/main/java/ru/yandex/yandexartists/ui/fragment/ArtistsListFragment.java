package ru.yandex.yandexartists.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import butterknife.BindView;
import icepick.Icepick;
import icepick.State;
import ru.yandex.yandexartists.R;
import ru.yandex.yandexartists.bean.Artist;
import ru.yandex.yandexartists.type.OnLoadingListener;
import ru.yandex.yandexartists.type.OnSelectedItem;
import ru.yandex.yandexartists.utils.AnimationUtils;
import ru.yandex.yandexartists.utils.adapter.ArtistsListAdapter;
import ru.yandex.yandexartists.utils.factory.ContentLoader;

/**
 * Created by root on 7/19/16.
 */
public class ArtistsListFragment extends BaseFragment {

    @BindView(R.id.swipe_refresh_layout) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.artists_list) @Nullable ListView artistsList;
    @BindView(R.id.artists_list_land) @Nullable GridView artistsListLand;
    @BindView(R.id.refresh_data) Button refreshButton;

    @State Artist[] artists;

    @State boolean isLoaded = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.artists_list_fragment, container, false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        setListenerToRefreshLayout();
        artists = (Artist[]) getArguments().getSerializable(Artist[].class.getCanonicalName());
        if(!isLoaded) {
            if (getArguments().getBoolean("requireRefresh")) {
                onRefreshListener(true).onRefresh();
            }
        }
        setArtists();
        AnimationUtils.movingAnimation(getView(), 1000, 0, 0, 0, 700, (animation) -> {}, (animation) -> {});

        refreshButton.setOnClickListener((v) -> {
            AnimationUtils.movingAnimation(v, 0, 0, 0, -250, 300, (animation) -> {}, (animation) -> v.setVisibility(View.GONE));
            isLoaded = true;
            setArtists();
        });

    }

    private void setArtists() {
        if(artists != null) {
            if (artistsList != null) {
                artistsList.setAdapter(new ArtistsListAdapter((OnSelectedItem<Artist>) getActivity(), artists));
            } else {
                artistsListLand.setAdapter(new ArtistsListAdapter((OnSelectedItem<Artist>) getActivity(), artists));
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        AnimationUtils.movingAnimation(getView(), 0, -1000, 0, 0, 700, (animation) -> {}, (animation) -> {});
    }

    private void setListenerToRefreshLayout() {
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener(false));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.artists_list_menu, menu);
    }

    private SwipeRefreshLayout.OnRefreshListener onRefreshListener(boolean showProgress) {
        return ()-> {
            swipeRefreshLayout.setRefreshing(true);
            ((OnLoadingListener) getActivity()).onStartLoading();

            ContentLoader.getArtists(getContext(), (json) -> {
                Artist[] artists = new Gson().fromJson(json, new TypeToken<Artist[]>() {
                }.getType());
                ((OnLoadingListener) getActivity()).onEndLoading(artists);
                swipeRefreshLayout.setRefreshing(false);
                if(showProgress) {
                    showDoneButton();
                }
                Log.d("artists", "done");
            }, (error) -> {
                ((OnLoadingListener) getActivity()).onFailedLoading();
                Log.d("artists", "fail");
                swipeRefreshLayout.setRefreshing(false);
            });
        };
    }

    private void showDoneButton() {
        AnimationUtils.movingAnimation(refreshButton, 0, 0, -250, 0, 300, (animation) -> refreshButton.setVisibility(View.VISIBLE), (animation) -> {});
    }

}
