package ru.yandex.yandexartists.utils;

import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

import ru.yandex.yandexartists.function.Consumer;

/**
 * Created by root on 4/25/16.
 */
public class AnimationUtils {
    
    public static void movingAnimation(View view, int fromX, int toX, int fromY, int toY, int duration, final Consumer<Animation> before, final Consumer<Animation> after) {
        Animation animation = new TranslateAnimation(fromX, toX, fromY, toY);
        animation.setDuration(duration);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                before.function(animation);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                after.function(animation);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(animation);
    }

    public static void alphaAnimation(View view, float from, float to , int duration, final Consumer<Animation> before, final Consumer<Animation> after) {
        Animation animation = new AlphaAnimation(from, to);
        animation.setDuration(duration);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                before.function(animation);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                after.function(animation);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(animation);
    }

}
