package ru.yandex.yandexartists.ui.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import icepick.State;
import ru.yandex.yandexartists.R;
import ru.yandex.yandexartists.bean.Artist;
import ru.yandex.yandexartists.utils.AnimationUtils;
import ru.yandex.yandexartists.utils.StringUtils;

/**
 * Created by root on 7/20/16.
 */
public class ArtistInfoFragment extends BaseFragment {

    @BindView(R.id.image) ImageView image;
    @BindView(R.id.link) TextView link;
    @BindView(R.id.genres) TextView genres;
    @BindView(R.id.counter) TextView counter;
    @BindView(R.id.bio_title) TextView bioTitle;
    @BindView(R.id.bio_text) TextView bioText;

    @State Artist artist;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.artist_info_fragment, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        artist = (Artist) getArguments().getSerializable(Artist.class.getCanonicalName());
        fillPage();
        enableHomeButton(true);
        AnimationUtils.movingAnimation(getView(), 1000, 0, 0, 0, 700, (animation) -> {}, (animation) -> {});
    }

    @Override
    public void onResume() {
        super.onResume();
        setActionBarTitle(artist.getName());
    }

    private void fillPage() {

        Picasso.with(getContext()).load(artist.getCover().getBig()).centerCrop().fit().into(image);

        genres.setText(StringUtils.getGenresString(artist.getGenres()));
        counter.setText(StringUtils.getCounterInfoString(getContext(), artist));
        bioText.setText(artist.getDescription());

        link.setOnClickListener((v) -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW);
            browserIntent.setData(Uri.parse(artist.getLink()));
            startActivity(browserIntent);
        });

    }

    @Override
    public void onStop() {
        super.onStop();
        AnimationUtils.movingAnimation(getView(), 0, 1000, 0, 0, 700, (animation) -> {}, (animation) -> {});
    }

}
