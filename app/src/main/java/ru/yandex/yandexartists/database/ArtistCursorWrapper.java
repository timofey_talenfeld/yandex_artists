package ru.yandex.yandexartists.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import ru.yandex.yandexartists.bean.Artist;
import ru.yandex.yandexartists.bean.Cover;

import static ru.yandex.yandexartists.database.ArtistDbSchema.ArtistTable.Cols.*;

/**
 * Created by root on 7/21/16.
 */
public class ArtistCursorWrapper extends CursorWrapper {
    /**
     * Creates a cursor wrapper.
     *
     * @param cursor The underlying cursor to wrap.
     */
    public ArtistCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Artist getArtist() {
        Artist artist = new Artist();
        artist.setName(getString(getColumnIndex(NAME)));
        artist.setGenres(getString(getColumnIndex(GENRES)).split(","));
        artist.setTracks(getInt(getColumnIndex(TRACKS)));
        artist.setAlbums(getInt(getColumnIndex(ALBUMS)));
        artist.setLink(getString(getColumnIndex(LINK)));
        artist.setDescription(getString(getColumnIndex(DESCRIPTION)));

        Cover cover = new Cover();
        cover.setBig(getString(getColumnIndex(BIG_COVER)));
        cover.setSmall(getString(getColumnIndex(SMALL_COVER)));

        artist.setCover(cover);

        return artist;
    }

}
