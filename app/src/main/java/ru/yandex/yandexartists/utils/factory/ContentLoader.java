package ru.yandex.yandexartists.utils.factory;

import android.content.Context;
import android.util.Pair;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;

import ru.yandex.yandexartists.function.Consumer;

/**
 * Created by root on 3/20/16.
 */
public class ContentLoader {

    public static final String LINK = "https://raw.githubusercontent.com/Pipboy200011/YM2016/master/app/src/main/assets/artists.json";

    public static void getArtists(Context context, final Consumer<String> successCallback, final Consumer<VolleyError> failCallback) {

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, LINK, (response) ->
                successCallback.function(response), (error) -> failCallback.function(error));

        requestQueue.add(stringRequest);
        requestQueue.start();
    }
}
