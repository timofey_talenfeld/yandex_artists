package ru.yandex.yandexartists.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import ru.yandex.yandexartists.bean.Artist;

/**
 * Created by root on 7/21/16.
 */
public class ArtistsStorage {
    private static ArtistsStorage ourInstance;

    private Context context;
    private SQLiteDatabase database;


    private ArtistsStorage(Context context) {
        this.context = context.getApplicationContext();
        database = new ArtistBaseHelper(context).getWritableDatabase();
    }

    public static ArtistsStorage getInstance(Context context) {
        if(ourInstance == null) {
            ourInstance = new ArtistsStorage(context);
        }
        return ourInstance;
    }

    public void addArtist(Artist artist) {
        ContentValues contentValues = getContentValues(artist);
        database.insert(ArtistDbSchema.ArtistTable.NAME, null, contentValues);
    }

    private ArtistCursorWrapper queryCrimes(String whereClause, String[] whereArgs) {
        Cursor cursor = database.query(ArtistDbSchema.ArtistTable.NAME, null, whereClause, whereArgs, null, null, null);
        return new ArtistCursorWrapper(cursor);
    }

    public void updateStorage(Artist[] artists) {
        Log.d("artists", "update");
        database.execSQL("delete from " + ArtistDbSchema.ArtistTable.NAME);
        for(Artist artist : artists) {
            addArtist(artist);
        }
    }

    public Artist[] getArtists() {
        List<Artist> artists = new ArrayList<>();
        ArtistCursorWrapper cursor = queryCrimes(null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                artists.add(cursor.getArtist());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return artists.toArray(new Artist[artists.size()]);
    }

    private static ContentValues getContentValues(Artist artist) {
        ContentValues values = new ContentValues();
        values.put(ArtistDbSchema.ArtistTable.Cols.NAME, artist.getName());

        StringBuilder allGenres = new StringBuilder();

        for(String genre : artist.getGenres()) {
            allGenres.append(genre);
            allGenres.append(",");
        }

        if(allGenres.length() > 0) {
            allGenres.delete(allGenres.length() - 1, allGenres.length());
        }

        values.put(ArtistDbSchema.ArtistTable.Cols.GENRES, allGenres.toString());
        values.put(ArtistDbSchema.ArtistTable.Cols.TRACKS, artist.getTracks());
        values.put(ArtistDbSchema.ArtistTable.Cols.ALBUMS, artist.getAlbums());
        values.put(ArtistDbSchema.ArtistTable.Cols.LINK, artist.getLink());
        values.put(ArtistDbSchema.ArtistTable.Cols.DESCRIPTION, artist.getDescription());
        values.put(ArtistDbSchema.ArtistTable.Cols.BIG_COVER, artist.getCover().getBig());
        values.put(ArtistDbSchema.ArtistTable.Cols.SMALL_COVER, artist.getCover().getSmall());

        return values;
    }

}
