package ru.yandex.yandexartists.ui.fragment;

import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mikhaellopez.circularfillableloaders.CircularFillableLoaders;

import butterknife.BindView;
import ru.yandex.yandexartists.asynctask.LoadFromDatabaseTask;
import ru.yandex.yandexartists.R;
import ru.yandex.yandexartists.bean.Artist;
import ru.yandex.yandexartists.type.OnLoadingListener;
import ru.yandex.yandexartists.utils.AnimationUtils;
import ru.yandex.yandexartists.utils.factory.ContentLoader;

/**
 * Created by root on 7/19/16.
 */
public class LoadingFragment extends BaseFragment {

    @BindView(R.id.progress_bar) CircularFillableLoaders progressBar;
    @BindView(R.id.title) TextView title;
    @BindView(R.id.created) TextView created;

    private AsyncTask<Void, Void, Artist[]> loadFromDBTask;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.loading_fragment, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        AnimationUtils.movingAnimation(getView(), 1000, 0, 0, 0, 700, (animation) -> {}, (animation) -> {});
        loadArtists();
    }

    private void loadArtists() {
        setLoadingRate(30);
        loadFromLocal();
        ((OnLoadingListener) getActivity()).onStartFirstLoading();

    }

    private void loadFromNetwork() {
        ContentLoader.getArtists(getContext(), (value) -> {
            Artist[] artists = new Gson().fromJson(value, new TypeToken<Artist[]>() {
            }.getType());
            ((OnLoadingListener) getActivity()).onEndFirstLoading(artists, false);
        }, (value) -> ((OnLoadingListener) getActivity()).onFailedFirstLoading());
    }

    private void loadFromLocal() {
        loadFromDBTask = new LoadFromDatabaseTask(getContext(), (artists) -> {
            if (artists == null || artists.length == 0) {
                loadFromNetwork();
            } else {
                ((OnLoadingListener) getActivity()).onEndFirstLoading(artists, true);
            }
        }).execute();
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        if(loadFromDBTask != null) {
            loadFromDBTask.cancel(true);
        }
        AnimationUtils.movingAnimation(getView(), 0, -1000, 0, 0, 700, (animation) -> {}, (animation) -> {});
    }

    public void setLoadingRate(int rate) {
        if(rate < 0) {
            rate = 0;
        }
        if(rate > 100) {
            rate = 100;
        }
        progressBar.setProgress(100 - rate);
    }



}
