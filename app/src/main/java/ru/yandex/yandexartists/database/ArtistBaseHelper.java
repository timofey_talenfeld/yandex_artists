package ru.yandex.yandexartists.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by root on 7/21/16.
 */
public class ArtistBaseHelper extends SQLiteOpenHelper {

    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "artistBase.db";

    public ArtistBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + ArtistDbSchema.ArtistTable.NAME + "(" +
                " _id integer primary key autoincrement, " +
                ArtistDbSchema.ArtistTable.Cols.NAME + ", " +
                ArtistDbSchema.ArtistTable.Cols.GENRES + ", " +
                ArtistDbSchema.ArtistTable.Cols.TRACKS + ", " +
                ArtistDbSchema.ArtistTable.Cols.ALBUMS + ", " +
                ArtistDbSchema.ArtistTable.Cols.LINK + ", " +
                ArtistDbSchema.ArtistTable.Cols.DESCRIPTION + ", " +
                ArtistDbSchema.ArtistTable.Cols.SMALL_COVER + ", " +
                ArtistDbSchema.ArtistTable.Cols.BIG_COVER +
                ")"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
