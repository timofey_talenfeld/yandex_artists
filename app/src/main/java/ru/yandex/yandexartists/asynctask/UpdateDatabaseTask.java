package ru.yandex.yandexartists.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import ru.yandex.yandexartists.bean.Artist;
import ru.yandex.yandexartists.database.ArtistsStorage;

/**
 * Created by root on 7/21/16.
 */
public class UpdateDatabaseTask extends AsyncTask<Void, Void, Void> {

    private Artist[] artists;
    private Context context;

    public UpdateDatabaseTask(Artist[] artists, Context context) {
        this.context = context.getApplicationContext();
        this.artists = artists;
    }

    @Override
    protected Void doInBackground(Void... params) {

        ArtistsStorage.getInstance(context).updateStorage(artists);

        return null;
    }
}
